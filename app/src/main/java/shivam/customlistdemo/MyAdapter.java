package shivam.customlistdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by shivam on 31/5/17.
 */

public class MyAdapter extends BaseAdapter {

    Context con;
    Integer[] cimg;
    String[] cname,cnumber;
    LayoutInflater li;
    ImageView iv;
    TextView t1,t2;


    public MyAdapter(Context con, Integer[] cimg, String[] cname, String[] cnumber) {
        this.con = con;
        this.cimg = cimg;
        this.cname = cname;
        this.cnumber = cnumber;
        li = LayoutInflater.from(con);
    }

    @Override
    public int getCount() {
        return cname.length;
    }

    @Override
    public Object getItem(int position) {
        return cimg;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v1 = li.inflate(R.layout.custom_list,null);

        iv = (ImageView) v1.findViewById(R.id.img);
        t1 = (TextView) v1.findViewById(R.id.name);
        t2 = (TextView) v1.findViewById(R.id.number);

        iv.setImageResource(cimg[position]);
        t1.setText(cname[position]);
        t2.setText(cnumber[position]);

        return v1;
    }
}
